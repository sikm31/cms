package ua.dp.cms.services;

import ua.dp.cms.model.Product;

import java.util.List;

/**
 * Created by y.voytovich on 05.10.2015.
 */
public interface ProductService {

    public Product getProduct(Long id);
    public void addProduct(Product product);
    public void deleteProduct(Long id);
    public void editProduct(Product product);
    public List<Product> getAllProduct();
}
