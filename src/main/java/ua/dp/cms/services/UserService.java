package ua.dp.cms.services;


import ua.dp.cms.model.User;

import java.util.List;

/**
 * Created by y.voytovich on 16.09.2015.
 */
public interface UserService {
    public void addUser(User user);
    public void deleteUser(Long id);
    public void editUser(User user);
    public List<User> getAll();
    public User getUserById(Long id);
    public User getUserByUserName(String userName);
    public void saveUser(User user);
    public boolean isUserExist(User user);


}
