package ua.dp.cms.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.cms.dao.UserDao;
import ua.dp.cms.model.User;


import java.util.List;

/**
 * Created by y.voytovich on 16.09.2015.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public boolean isUserExist(User user) {
        return getUserByUserName(user.getUsername())!=null;
    }
    @Override
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Override
    public void deleteUser(Long id) {
        userDao.deleteUser(id);
    }

    @Override
    public void editUser(User user) {
            userDao.editUser(user);
    }

    @Override
    public List<User> getAll() {

        return userDao.getAll();
    }

    @Override
    public User getUserById(Long id) {

        return userDao.getUserById(id);
    }

    @Override
    public User getUserByUserName(String userName) {

        return userDao.getUserByUserName(userName);
    }

    @Override
    public void saveUser(User user) {
        userDao.saveUser(user);
    }
}
