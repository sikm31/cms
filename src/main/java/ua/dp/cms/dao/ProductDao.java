package ua.dp.cms.dao;

import ua.dp.cms.model.Product;

import java.util.List;

/**
 * Created by y.voytovich on 03.08.2015.
 */
public interface ProductDao {

    public Product getProduct(Long id);
    public void addProduct(Product product);
    public void deleteProduct(Long id);
    public void editProduct(Product product);
    public List<Product> getAllProduct();

}
