package ua.dp.cms.dao;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.cms.model.Product;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by y.voytovich on 07.08.2015.
 */
@Repository
@Transactional
public class ProductDaoImpl implements ProductDao {

    Logger logger = LoggerFactory.getLogger(ProductDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public Product getProduct(Long id) {
        logger.debug("get id poduct");
        Session session = sessionFactory.getCurrentSession();
        Product product = (Product) session.get(Product.class, id);
        return product;
    }

    @Override
    public void addProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.save(product);
    }

    @Override
    public void deleteProduct(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Product product = (Product) session.get(Product.class, id);
        session.delete(product);
    }

    @Override
    public void editProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        Product existingProduct = (Product) session.get(Product.class, product.getId());
        existingProduct.setName(product.getName());
        existingProduct.setArticle(product.getArticle());
        existingProduct.setCost(product.getCost());
        existingProduct.setImage(product.getImage());
        existingProduct.setCost(product.getCost());
        existingProduct.setShortdescriprion(product.getShortdescriprion());
        existingProduct.setFulldescription(product.getFulldescription());
        session.save(existingProduct);
    }

    @Override
    public List<Product> getAllProduct() {
        logger.debug("list all metadata");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Product");
        return query.list();
    }


}
