package ua.dp.cms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.dp.cms.model.Product;
import ua.dp.cms.model.User;
import ua.dp.cms.services.ProductService;
import ua.dp.cms.services.UserService;

/**
 * Created by y.voytovich on 05.10.2015.
 */
@Controller
@RequestMapping("/fill")
public class FillController {

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    @RequestMapping(method = RequestMethod.GET)
    public void fillDB() {



            userService.addUser(new User("evgen", "123", true, "ROLE_ADMIN"));
            userService.addUser(new User("yuri", "123", true, "ROLE_ADMIN"));
            userService.addUser(new User("dan", "123", true, "ROLE_ADMIN"));
            userService.addUser(new User("mkyoung", "123", true, "ROLE_USER"));

            productService.addProduct(new Product("name", "image", "article", 100, "sdfsfsdfsdfsdfsdf", "sdfsdfsdf"));
            productService.addProduct(new Product("name", "image", "article", 100, "sdfsfsdfsdfsdfsdf", "sdfsdfsdf"));
            productService.addProduct(new Product("name", "image", "article", 100, "sdfsfsdfsdfsdfsdf", "sdfsdfsdf"));
            productService.addProduct(new Product("name", "image", "article", 100, "sdfsfsdfsdfsdfsdf", "sdfsdfsdf"));

    }
}
