package ua.dp.cms.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.cms.model.Product;
import ua.dp.cms.services.ProductService;

import java.util.List;

/**
 * Created by y.voytovich on 07.08.2015.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    Logger logger = LoggerFactory.getLogger(AdminController.class);
//
//    @Autowired
//    ProductService productService;
//
//
//    @RequestMapping(method = RequestMethod.GET)
//    public ModelAndView add() {
//        return new ModelAndView("admin/admin", "product", new Product());
//    }
//
//    @RequestMapping(value = "/addproduct", method = RequestMethod.GET)
//    public ModelAndView addproduct() {
//        return new ModelAndView("admin/addproduct", "product", new Product());
//    }
//
//    @RequestMapping(value = "/addproduct", method = RequestMethod.POST)
//    public String addproductPost(Product product, Model model) {
//        productService.addProduct(product);
//        model.addAttribute("product", product);
//        return "admin/productsucces";
//    }
//
//    @RequestMapping(value = "/allproduct", method = RequestMethod.GET)
//    public String getAllContent(Model model) {
//        logger.debug("Received request to show all content");
//        List<Product> productList = productService.getAllProduct();
//        model.addAttribute("productList", productList);
//        return "admin/allproduct";
//    }
//
//    @RequestMapping(value = "/delproduct", method = RequestMethod.GET)
//    public String deleteMetadata(@RequestParam(value = "id", required = true) Integer id, Model model) {
//        logger.debug("Delete Metadata" + id);
//        productService.deleteProduct(id);
//        model.addAttribute("id", id);
//        return "admin/delproduct";
//    }
//
//    @RequestMapping(value = "/editproduct", method = RequestMethod.GET)
//    public String editMetadata(@RequestParam(value = "id", required = true) Integer id, Model model) {
//        logger.debug("Edit Product" + id);
//
//        model.addAttribute("productAttribute", productService.getProduct(id));
//        return "admin/editproduct";
//    }
//
//    @RequestMapping(value = "/editproduct", method = RequestMethod.POST)
//    public String saveEditMetadata(@ModelAttribute("productAttribute") Product product,
//                                   @RequestParam(value = "id", required = true) Integer id,
////                                   @RequestParam(value = "path", required = true) String path,
////                                   @RequestParam(value = "title", required = true) String title,
////                                   @RequestParam(value = "content", required = true) String content,
////                                   @RequestParam(value = "template", required = true) String template,
//                                   Model model) {
//        logger.debug("Edit save Metadata" + id);
//        product.setId(id);
//        productService.editProduct(product);
//        model.addAttribute("id", id);
//        return "admin/editedproduct";
//    }


    @RequestMapping(method = RequestMethod.GET)
    public String getAdminPage() {
        return "admin/admin";
    }


    @RequestMapping(value = "/productmanage",method = RequestMethod.GET)
    public String getProductPage() {
        return "admin/ProductManagement";
    }

    @RequestMapping(value = "/usermanage",method = RequestMethod.GET)
    public String getUserPage() {
        return "admin/UserManagement";
    }

}
