package ua.dp.cms.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by y.voytovich on 03.08.2015.
 */
@Entity
@Table(name = "product")
public class Product {

    @Id
    @Column(name = "product_id")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long id;

    @Column(name = "product_name", nullable = false)
    private String name;


    @Column(name="product_image", nullable=false)
    private String image;

    @Column(name = "product_article", nullable = true)
    private String article;

    @Column(name = "product_cost", nullable = true)
    private Integer cost;

    @Column(name = "product_shortdesc", nullable = true)
    private String shortdescriprion;

    @Column(name = "product_fulldesc", nullable = false)
    private String fulldescription;

    public Product(){}

    public Product(String name, String image, String article, Integer cost, String shortdescriprion, String fulldescription) {
        this.name = name;
        this.image = image;
        this.article = article;
        this.cost = cost;
        this.shortdescriprion = shortdescriprion;
        this.fulldescription = fulldescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getShortdescriprion() {
        return shortdescriprion;
    }

    public void setShortdescriprion(String shortdescriprion) {
        this.shortdescriprion = shortdescriprion;
    }

    public String getFulldescription() {
        return fulldescription;
    }

    public void setFulldescription(String fulldescription) {
        this.fulldescription = fulldescription;
    }
}
