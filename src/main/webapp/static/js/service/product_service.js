'use strict';

App.factory('ProductService', ['$http', '$q', function($http, $q){

    return {

        getAllProduct: function() {
            return $http.get('/product/')
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while fetching product');
                    return $q.reject(errResponse);
                }
            );
        },

        addProduct: function(product){
            return $http.post('/product/', product)
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while creating product');
                    return $q.reject(errResponse);
                }
            );
        },

        editProduct: function(product, id){
            return $http.put('/product/'+id, product)
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while updating product');
                    return $q.reject(errResponse);
                }
            );
        },

        deleteProduct: function(id){
            return $http.delete('/product/'+id)
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while deleting product');
                    return $q.reject(errResponse);
                }
            );
        }

    };

}]);
