<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>

  <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
  <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
  <link href="<c:url value='/static/css/main.css' />" rel="stylesheet"></link>
</head>
<body ng-app="myApp" class="ng-cloak">
<div class="generic-container" ng-controller="ProductRestController as ctrl">



      <table class="table table-hover">
        <thead>
        <tr>
          <th>ID.</th>
          <th>Name</th>
          <th>Image</th>
          <th>Article</th>
          <th>Cost</th>
          <th>Shortdescriprion</th>
          <th>Fulldescription</th>
          <th width="20%"></th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="p in ctrl.product">
          <td><span ng-bind="p.id"></span></td>
          <td><span ng-bind="p.name"></span></td>
          <td><span ng-bind="p.image"></span></td>
          <td><span ng-bind="p.article"></span></td>
          <td><span ng-bind="p.cost"></span></td>
          <td><span ng-bind="p.shortdescriprion"></span></td>
          <td><span ng-bind="p.fulldescription"></span></td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>




<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/static/js/app.js' />"></script>
<script src="<c:url value='/static/js/service/product_service.js' />"></script>
<script src="<c:url value='/static/js/controller/product_controller.js' />"></script>
</body>
</html>